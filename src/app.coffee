express = require('express')                            #Web Server
path = require('path')                                  #Node Path Helper
favicon = require('serve-favicon')                      #Favicon Helper
logger = require('morgan')                              #Debugging Logging
cookieParser = require('cookie-parser')                 #Read/Write Cookies
bodyParser = require('body-parser')                     #Read/Write Req PARAMS
routes = require('./routes/index')                      #Main Site Routes
app = express()                                         #Create App


app.set 'views', path.join(__dirname, 'views')          #Alias views to /
app.set 'view engine', 'jade'                           #And use Jade

##Don't Change the Order Kyle. You'll screw something
##up like last time.

#app.use(favicon(__dirname + '/public/favicon.ico'));   #Serve FavIcon
app.use logger('dev')                                   #Log info to console
app.use bodyParser.json()                               #Parse JSON
app.use bodyParser.urlencoded(extended: false)          #Parse POST
app.use cookieParser()                                  #Read Cookies
app.use express.static(path.join(__dirname, 'public'))  #Alias public to /
app.use '/', routes                                     #Main Site Routes

# catch 404 and forward to error handler
app.use (req, res, next) ->
  err = new Error('Not Found')
  err.status = 404
  next err
  return

# error handlers

# development error handler
# will print stacktrace
if app.get('env') is 'development'
  app.use (err, req, res, next) ->
    res.status err.status or 500
    res.render 'error',
      message: err.message
      error: err

    return



# production error handler
# no stacktraces leaked to user
app.use (err, req, res, next) ->
  res.status err.status or 500
  res.render 'error',
    message: err.message
    error: {}

  return

app.set 'port', process.env.PORT or 3000
server = app.listen(app.get('port'), ->
  console.log 'Express server listening on port ' + server.address().port
  return
)
