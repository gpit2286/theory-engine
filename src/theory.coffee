theoryEngine = (canvasName, opts) ->
  opts = extend
    height: 200
    width: 550
    xOffset: 0
  , opts

  #Get the canvas
  canvas = document.getElementById canvasName

  #Set the height and width of the canvas
  canvas.setAttribute 'height', opts.height + 'px'
  canvas.setAttribute 'width', opts.width + 'px'

  #Get the context to start drawing on Canvas
  ctx = canvas.getContext('2d')

  #Staff lines exist in the middle of the staff, with a
  #10% margin on the sides.
  ctx.translate (opts.width*.1), (opts.height*.5)

  #Step size. This is the size of space from middle
  #of one note to the middle of the next, vertically
  staffSpace = Math.ceil opts.height / 24

  #Draw the Staff
  ctx.beginPath()
  for x in [-2..2]
    ctx.moveTo 0, x * staffSpace * 2
    ctx.lineTo opts.width * .8, x * staffSpace * 2
  ctx.stroke()

  #The base font size for the paganini font
  ctx.font = (opts.height/3) + 'px paganini'

  #Start adding things to the staff
  #First the clef
  if opts.clef
    switch opts.clef
      when 'alto'
        #The c-clef without and modification sits one note off.
        #Since the lines are calculated at 1/12 of the canvas height,
        #one note is 1/24. The +1 is to fix a pixel. Don't know
        #how it will affect scaling yet
        ctx.fillText '\ue16f', opts.xOffset, staffSpace
      when 'tenor'
        ctx.fillText '\ue16f', opts.xOffset, staffSpace * -1
      when 'treble'
        ctx.fillText '\ue173', opts.xOffset, staffSpace * 3
      when 'bass'
        ctx.fillText '\ue171', opts.xOffset, staffSpace * -1
      when 'perc'
        ctx.fillText '\ue175', opts.xOffset, staffSpace
      when 'tab'
        ctx.fillText '\ue177', opts.xOffset, staffSpace
      else
        console.log 'Incorrect Clef selection entered...'
    #Increment the xOffset to the next item starts in the right place
    opts.xOffset = opts.xOffset + (opts.height/3) * .82

  #Then the key signature
  if opts.key
    #split the option into the key and major/minor.
    opts.key = opts.key.split ' '
    keySigData =
      count:
        a: 3
        aes: -4
        #ais
        b: 5
        bes: -2
        #bis
        c: 0
        cis: 7
        ces: -7
        d: 2
        des: -5
        #dis
        e: 4
        ees: -3
        #eis
        f: -1
        fis: 6
        #fes
        g: 1
        #gis
        ges: -6
      pos:
        sharp:
          treble: [-4, -1, -5, -2, 1, -3, 0]
          bass: [-2, 1, -3, 0, 3, -1, 2]
          alto: [-3, 0, -4, -1, 2, -2, 1]
          tenor: [2, -2, 1, -3, 0, -4, -1]
        flat:
          treble: [0, -3, 1, -2, 2, -1, 3]
          bass: [2, -1, 3, 0, 4, 1, 5]
          alto: [1, -2, 2, -1, 3, 0, 4]
          tenor: [-1, -4, 0, -3, 1, -2, 2]

    #A negative number indicates flats. A positive number is sharps
    accTotal = keySigData.count[opts.key[0]]
    #If minor is in the string, then we need to decrease the
    #accidental count by 3. Math + Magic
    if opts.key[1]? and opts.key[1] is 'minor' then accTotal = accTotal - 3

    if accTotal > 0 #We have Sharps
      for x in [0..accTotal]
        #This is an ugly line.
        #staffSpace is the 24 divisions from before
        ctx.fillText '\ue10f', opts.xOffset, staffSpace * keySigData.pos.sharp[opts.clef][x] + staffSpace
        opts.xOffset = opts.xOffset + (opts.height/3) * .35
    else if accTotal < 0 #We have flats
      for x in [0..Math.abs(accTotal)]
        #See Notes above
        ctx.fillText '\ue11b', opts.xOffset, staffSpace * keySigData.pos.flat[opts.clef][x] + staffSpace
        opts.xOffset = opts.xOffset + (opts.height/3) * .25
    #If accTotal is 0, which is not caught here, we have no sharps or flats






extend = (out) ->
  out = out or {}

  for argument in arguments
    if !argument then continue

    for key of argument
      if argument.hasOwnProperty(key)
        out[key] = argument[key]

  return out;
