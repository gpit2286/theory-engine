var extend, theoryEngine;

theoryEngine = function(canvasName, opts) {
  var accTotal, canvas, ctx, keySigData, staffSpace, x, _i, _j, _k, _ref, _results, _results1;
  opts = extend({
    height: 200,
    width: 550,
    xOffset: 0
  }, opts);
  canvas = document.getElementById(canvasName);
  canvas.setAttribute('height', opts.height + 'px');
  canvas.setAttribute('width', opts.width + 'px');
  ctx = canvas.getContext('2d');
  ctx.translate(opts.width * .1, opts.height * .5);
  staffSpace = Math.ceil(opts.height / 24);
  ctx.beginPath();
  for (x = _i = -2; _i <= 2; x = ++_i) {
    ctx.moveTo(0, x * staffSpace * 2);
    ctx.lineTo(opts.width * .8, x * staffSpace * 2);
  }
  ctx.stroke();
  ctx.font = (opts.height / 3) + 'px paganini';
  if (opts.clef) {
    switch (opts.clef) {
      case 'alto':
        ctx.fillText('\ue16f', opts.xOffset, staffSpace);
        break;
      case 'tenor':
        ctx.fillText('\ue16f', opts.xOffset, staffSpace * -1);
        break;
      case 'treble':
        ctx.fillText('\ue173', opts.xOffset, staffSpace * 3);
        break;
      case 'bass':
        ctx.fillText('\ue171', opts.xOffset, staffSpace * -1);
        break;
      case 'perc':
        ctx.fillText('\ue175', opts.xOffset, staffSpace);
        break;
      case 'tab':
        ctx.fillText('\ue177', opts.xOffset, staffSpace);
        break;
      default:
        console.log('Incorrect Clef selection entered...');
    }
    opts.xOffset = opts.xOffset + (opts.height / 3) * .82;
  }
  if (opts.key) {
    opts.key = opts.key.split(' ');
    keySigData = {
      count: {
        a: 3,
        aes: -4,
        b: 5,
        bes: -2,
        c: 0,
        cis: 7,
        ces: -7,
        d: 2,
        des: -5,
        e: 4,
        ees: -3,
        f: -1,
        fis: 6,
        g: 1,
        ges: -6
      },
      pos: {
        sharp: {
          treble: [-4, -1, -5, -2, 1, -3, 0],
          bass: [-2, 1, -3, 0, 3, -1, 2],
          alto: [-3, 0, -4, -1, 2, -2, 1],
          tenor: [2, -2, 1, -3, 0, -4, -1]
        },
        flat: {
          treble: [0, -3, 1, -2, 2, -1, 3],
          bass: [2, -1, 3, 0, 4, 1, 5],
          alto: [1, -2, 2, -1, 3, 0, 4],
          tenor: [-1, -4, 0, -3, 1, -2, 2]
        }
      }
    };
    accTotal = keySigData.count[opts.key[0]];
    if ((opts.key[1] != null) && opts.key[1] === 'minor') {
      accTotal = accTotal - 3;
    }
    if (accTotal > 0) {
      _results = [];
      for (x = _j = 0; 0 <= accTotal ? _j <= accTotal : _j >= accTotal; x = 0 <= accTotal ? ++_j : --_j) {
        ctx.fillText('\ue10f', opts.xOffset, staffSpace * keySigData.pos.sharp[opts.clef][x] + staffSpace);
        _results.push(opts.xOffset = opts.xOffset + (opts.height / 3) * .35);
      }
      return _results;
    } else if (accTotal < 0) {
      _results1 = [];
      for (x = _k = 0, _ref = Math.abs(accTotal); 0 <= _ref ? _k <= _ref : _k >= _ref; x = 0 <= _ref ? ++_k : --_k) {
        ctx.fillText('\ue11b', opts.xOffset, staffSpace * keySigData.pos.flat[opts.clef][x] + staffSpace);
        _results1.push(opts.xOffset = opts.xOffset + (opts.height / 3) * .25);
      }
      return _results1;
    }
  }
};

extend = function(out) {
  var argument, key, _i, _len;
  out = out || {};
  for (_i = 0, _len = arguments.length; _i < _len; _i++) {
    argument = arguments[_i];
    if (!argument) {
      continue;
    }
    for (key in argument) {
      if (argument.hasOwnProperty(key)) {
        out[key] = argument[key];
      }
    }
  }
  return out;
};
