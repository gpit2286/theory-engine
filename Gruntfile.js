module.exports = function(grunt) {
  grunt.loadNpmTasks('grunt-contrib-coffee');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-cssmin');

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    coffee: {
      backEnd: {
        dest: 'app.js',
        src: 'src/app.coffee'
      },
      frontEnd: {
        options: {
          bare: true
        },
        files: {
          'public/js/theory.js': 'src/theory.coffee'
        }
      }
    },
    watch: {
      coffeeBackEnd: {
        files: ['src/app.coffee'],
        tasks: ['coffee:backEnd']
      },
      coffeeFrontEnd: {
        files: ['src/theory.coffee'],
        tasks: ['coffee:frontEnd']
      }
    },
    copy: {
      depJquery: {
        dest: 'public/js/jquery.js',
        src: 'bower_components/jquery/dist/jquery.min.js'
      },
      depFont: {
        dest: 'public/js/fontloader.js',
        src: 'bower_components/fontloader/FontLoader.js'
      },
      depBootstrap: {
        files: [
          {
            dest: 'public/js/bootstrap.js',
            src: 'bower_components/bootstrap/dist/js/bootstrap.min.js'
          },
          {
            dest: 'public/fonts/',
            src: ['bower_components/bootstrap/dist/fonts/*'],
            expand: true,
            flatten: true
          }
        ]
      }
    },
    cssmin: {
      depComplete: {
        files: [
          {
            dest: 'public/css/dep.css',
            src: ['bower_components/bootstrap/dist/css/bootstrap.css']
          }
        ]
      }
    }
  });

  grunt.registerTask('build-dep', function() {
    grunt.task.run(['copy:depJquery', 'copy:depBootstrap',
              'copy:depFont', 'cssmin:depComplete']);


  });
  grunt.registerTask('default', ['coffee']);

};
